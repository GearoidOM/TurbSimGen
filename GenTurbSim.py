import argparse
from pathlib import Path
import openpyxl as pyxl
import os
from tqdm import tqdm
from TurbSimIn import TurbSim
from InflowWindIn import InflowWind

def update_line(line,tag,update):
    idx = line.index(tag)
    return update.ljust(idx,' ') + line[idx:]

def write_out_file(data,fname):
    with open(fname,'w+') as fout:
        for line in data.values():
            fout.write(line)

def main(args):
    print("Processing Variations....")
    print(f"Input File: {args.input}")
    print(f"Output Directory: {args.output}")

    try:
        file_path = Path(args.input).resolve(strict=True)
    except FileNotFoundError as fnferr:
        print(f"Error: {args.input} not found, {fnferr}")
    else:

        if not args.output or not Path(args.output).exists():
            output_dir = os.getcwd()
        else:
            output_dir = Path(args.output).resolve(strict=True)
    
            wb = pyxl.load_workbook(filename=file_path, data_only=True)

            sheet = wb["TurbSim"]

            param_variations = {col[0]: col[1:] for col in zip(*sheet.values)}

            num_variations = len(param_variations[next(iter(param_variations))])

            turbSimIn = TurbSim()
            
            InflowWindIn = InflowWind()

            TurbSim_out_file_data = turbSimIn.input

            InflowWind_dat_file_data = InflowWindIn.input

            propagation_direction = "PropagationDir"
                    
            for ivar in tqdm(range(num_variations)):
                for key in param_variations:
                    if key in TurbSim_out_file_data.keys():
                        TurbSim_out_file_data[key] = update_line(TurbSim_out_file_data[key],key,str(param_variations[key][ivar]))

                InflowWind_dat_file_data[propagation_direction] = \
                    update_line(InflowWind_dat_file_data[propagation_direction],propagation_direction,str(param_variations[propagation_direction][ivar]*-1.0))

                file_name = (
                    f"{param_variations['TurbModel'][ivar]}-"
                    f"{param_variations['IEC_WindType'][ivar]}-"
                    f"{param_variations['URef'][ivar]}-"
                    f"{param_variations['RandSeed1'][ivar]}-"
                    f"{param_variations['PropagationDir'][ivar]}")
                      
                # Turbsim
                TurbSim_out_file = os.path.join(output_dir, file_name + '.in') 
                write_out_file(TurbSim_out_file_data,TurbSim_out_file)
                TurbSim_out_file_data = turbSimIn.input

                # InflowWInd
                InflowWind_dat_file_data['FilenameBinary'] = update_line(InflowWind_dat_file_data['FilenameBinary'],'Filename',file_name +'.bts')

                InflowWind_dat_file = os.path.join(output_dir, file_name + '.dat')
                write_out_file(InflowWind_dat_file_data,InflowWind_dat_file)
                InflowWind_dat_file_data = InflowWindIn.input

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--input', help='Path to Excel file with TurbSim parameter variations')
    parser.add_argument('--output', help='Path to folder to write modified TurbSim files to')
    args = parser.parse_args()
    main(args)
